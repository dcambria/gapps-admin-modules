#!/usr/bin/env python3
#
# Ferramentas para manipular rapidamente arquivos csv
# Por Daniel Cambría
# Versão: 0.5
# Última atualização: 2020-02-06

from module_bcolors import bcolors 
import csv


def append_csv(filename, fromlist): 
    """ Cria novo arquivo CSV e faz append.
    Evita destruição das linhas já gravadas no arquivo original.
    """
    
    try:
        with open(filename, 'a', newline='', encoding="UTF-8") as f:
            writer = csv.writer(f)
            writer.writerows(fromlist)
        f.close()        
        return 200
    except:
        print('Erro')
        return False
        
        
def write_csv(filename, fromlist):
    """ Cria novo arquivo CSV ou sobrescreve o atual, e grava linhas. 
    """
    
    try:
        with open(filename, 'w', newline='', encoding="UTF-8") as f:
            writer = csv.writer(f)
            writer.writerows(fromlist)
        f.close()        
        return 200
    except:
        print('Erro')
        return False

def import_csv(filename):  
    """ Abre o arquivo formato CSV e armazena em lista []
    Retorna lista, número de linhas
    """
    
    #conta o total de linhas
    with open(filename, encoding="UTF-8") as csvfile:
        total_rows = sum(1 for line in csvfile)

    print(bcolors.HEADER + "Lendo arquivo: " + filename + bcolors.ENDC)
    print(bcolors.BOLD + "Total de linhas: " + str(total_rows) + bcolors.ENDC)
    
    #grava em imported_list
    imported_list = []
    with open(filename, encoding="UTF-8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for i, row in enumerate(readCSV):
            imported_list.append(row) # lê apenas a coluna 1 da lista
            print("Lendo: " + str(i + 1) + "/" + str(total_rows) + " " + str(row))
    csvfile.close()
    
    return imported_list, total_rows