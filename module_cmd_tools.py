#!/usr/bin/env python3
#
# Ferramentas para manipular rapidamente arquivos csv
# Por Daniel Cambría
# Versão: 0.5
# Última atualização: 2020-02-06

import subprocess
import re, os
from module_bcolors import bcolors

def console(cmd):                  
    """"Facilita a execução de comandos do shell
    """
    
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out, err = p.communicate()
    return (p.returncode, out, err)

def version_check():
    """Realiza a verificação de versão do programa.
    """
    
    git_fetch = 'git -C ' + "./" + ' fetch origin'
    git_newlog = 'git -C ' + "./" + ' log HEAD..origin/master --oneline'
    
    git_fetch=console(git_fetch)[1]
    git_newlog=console(git_newlog)[1]
    
    if git_fetch == git_newlog:
        print("")
        print(bcolors.OKBLUE + '====> ☕️ Up-to-date' + bcolors.ENDC)
        print("")
    else:
        print("")        
        print(bcolors.OKGREEN + "====> ☕️ NOVA VERSÃO! Atualize agora com mailcheck --update" + bcolors.ENDC)
        print("")

def dont_overwrite_filename(filename, prefix):
    """ Caso o arquivo já exista, evita que seja sobrescrito.
    Cria versões com prefixo e autoincrement de número.
    """
    
    # Incrementa +1 a partir do número 2
    count = 2
    
    # Quebra o caminho
    pathsplit = re.findall(r'([^\\]+\/)(.*)', filename)
            
    while True:
        file_exists = os.path.isfile(filename)
        if file_exists:
            filename = pathsplit[0][0] + prefix + '(' + str(count) + ')-' + pathsplit[0][1]     
            count += 1
        else:
            return filename
        
