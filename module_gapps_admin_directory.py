#!/usr/bin/env python3

# Implementação de vários métodos do Google Admin Directory API
# para Administradores GApps.
#
#
# Por: Daniel Cambría
# Criado em: 2020-02-03
# Última revisão:2020-02-04
# Versão: 0.5
# 
# Funções comentadas usando Docstrings https://www.datacamp.com/community/tutorials/docstrings-python#first-head

# Importa dependência de autenticação do Google APIs
from apiclient import discovery
from module_gappsapi_superadminauth import get_google_credentials
import httplib2

import random
import string

# Estes dois parâmetros devem ser iguais aos que foram setados nos IDs do cliente OAuth 2.0 
# https://console.cloud.google.com/apis/credentials
# Baixe o arquivo antes e salve no diretório do aplicativo
# Sete o mesmo nome em CLIENT_SECRET_FILE
CLIENT_SECRET_FILE = 'client_secrets.json'
APPLICATION_NAME = 'GAuth - Bureau'

# Define o diretório e arquivo do arquivo de credentials
# Este será o nome do arquivo de retorno do Google que credenciadá a conexão
credential_file = 'googleapis.com-python-gauth-bureau.json'

# Escopos permitidos para o credentials_file trabalhar.
# Se modificar o escopo, delete o arquivo setado em credential_file
# Deve ser semelhante ao definido em https://console.cloud.google.com/apis/credentials
scopes = ['https://www.googleapis.com/auth/admin.directory.user',
          'https://www.googleapis.com/auth/admin.directory.user.readonly',
          'https://www.googleapis.com/auth/admin.directory.group',
          'https://www.googleapis.com/auth/admin.directory.group.readonly',
          'https://www.googleapis.com/auth/admin.directory.group.member',
          'https://www.googleapis.com/auth/admin.directory.group.member.readonly',
          'https://www.googleapis.com/auth/admin.directory.orgunit',
          'https://www.googleapis.com/auth/admin.directory.orgunit.readonly',
          'https://www.googleapis.com/auth/admin.directory.domain',
          'https://www.googleapis.com/auth/admin.directory.domain.readonly',
          'https://www.googleapis.com/auth/admin.directory.userschema',
          # Retirar daqui esses scopes
          'https://www.googleapis.com/auth/spreadsheets',
          'https://www.googleapis.com/auth/youtube',
          'https://www.googleapis.com/auth/youtube.force-ssl',
          ]

# Recebe as credenciais OAuth2 do Google
credentials = get_google_credentials(scopes, CLIENT_SECRET_FILE, APPLICATION_NAME, credential_file)
http = credentials.authorize(httplib2.Http())

# Admin Directory API
# https://developers.google.com/resources/api-libraries/documentation/admin/directory_v1/python/latest/index.html
# https://www.googleapis.com/admin/directory/v1

# Lista de APIs possíveis
# https://github.com/googleapis/google-api-python-client/blob/master/docs/dyn/index.md

# Separar as APIs abaixo em outros arquivos
#
# http://googleapis.github.io/google-api-python-client/docs/dyn/sheets_v4.html
#sheets = discovery.build('sheets', 'v4', http=http)
# http://googleapis.github.io/google-api-python-client/docs/dyn/youtube_v3.html
#youtube = discovery.build('youtube', 'v3', http=http)
#http://googleapis.github.io/google-api-python-client/docs/dyn/drive_v3.html
#drive = discovery.build('drive', 'v3', http=http)

# Conecta com a API Admin Directory V1
admin_directory_v1 = discovery.build('admin', 'directory_v1', http=http)

# Váriáveis para testes rápidos
customer = "my_customer"

# Standart parameters:
# fields=None
#
# https://developers.google.com/admin-sdk/directory/v1/parameters

############################################################################################
# Method: orgunits()
############################################################################################

orgunits_method = admin_directory_v1.orgunits()

def orgunits_list(customerId=None, orgUnitPath=None, type=None, fields=None):
    """Retorna lista de Organizations.
    
    Referência: https://developers.google.com/apis-explorer/#search/organizations/m/admin/directory_v1/directory.orgunits.list
    
    orgUnitPath: '/' para toda a estrutura de diretórios
    type: all, children
    
    #
    # DICA: Para lista simples, use: fields = "organizationUnits/orgUnitPath"
    #
    """
    
    print('DICA: Para lista simples, use: fields = "organizationUnits/orgUnitPath", type = "all" ')
    
    try:
        results = orgunits_method.list(
                customerId = customer,
                orgUnitPath = orgUnitPath, # '/' para toda a estrutura de diretórios
                type = type, # all, children
                fields = fields # etag,kind,organizationUnits
                ).execute()
        return results
    
    except:
        print('Verifique o preenchimento correto dos parâmetros')                
        return False    

############################################################################################
# Method: domains()
############################################################################################

domains_method = admin_directory_v1.domains()

def domains_list(fields=None): 
    """Informa se um usuário é membro de grupo
    # https://developers.google.com/admin-sdk/directory/v1/reference/members/delete
    
    # Parâmetros da API
    #"""
    
    try:
        results = domains_method.list(             # Consulta na API
            customer='my_customer', 
            ).execute()
        
        print('Domínios principais:')
        for domain in results['domains']:
            print(domain['domainName'])

        return results

    except:
        print('Erro')
        return False


############################################################################################
# Method: members()
############################################################################################

members_method = admin_directory_v1.members()

def members_insert(groupKey, userKey, groupRole=None, fields=None):
    '''Insere membro em grupo
    
    # roles = MANAGER, OWNER, MEMBER
    #
    # https://developers.google.com/admin-sdk/directory/v1/reference/members/insert
    '''
    
    isMember = members_hasMember(groupKey, userKey)
    if isMember['isMember'] == True:
        print("Usuário já existe no grupo.")
        return False
    
    if groupRole == None:
        groupRole = input('Defina a permissão. [ENTER] para MEMBER\n\nOPÇÕES: \nMANAGER, \nOWNER, \nMEMBER\n\n> ') or 'MEMBER'    # MANAGER, OWNER, MEMBER
    
    body={ 
        'email' : userKey,
        'role': groupRole      # MANAGER, MEMBER, OWNER
           }
    
    try:
        results = members_method.insert(                          # Consulta na API
            groupKey=groupKey, 
            body = body
            ).execute()
        
        print(userKey + " incluído em " + groupKey)
        return results
    
    except:
        print('Verifique o preenchimento correto dos parâmetros')        
        return False

def members_delete(groupKey, memberKey, fields=None):
    '''Deleta membro de grupo
    # https://developers.google.com/admin-sdk/directory/v1/reference/members/delete
    
    '''
    
    isMember = members_hasMember(groupKey, memberKey)
    if isMember == True:
        
        try:
            results = members_method.delete(             # Consulta na API
                      groupKey=groupKey,
                      memberKey=memberKey
                      ).execute()
            print(str(isMember) + "--" + memberKey + " excluído de " + groupKey)
            return True, results
        
        except:
            print('Erro')
            return False
    else:
        print('Usuário não pertence ao grupo.')
        return False
    
def members_hasMember(groupKey, memberKey, fields=None):
    """Informa se um usuário é membro de grupo
    
    https://developers.google.com/admin-sdk/directory/v1/reference/members/delete
    Parâmetros da API
    
    """ 
    try:
        results = members_method.hasMember(             # Consulta na API
            groupKey=groupKey, 
            memberKey=memberKey
            ).execute()
        return results

    except:
        print('E-mail ou grupo inválido. Verifique o preenchimento dos dados.')
        return False

def members_list(groupKey, roles=None, includeDerivedMembership=None, fields=None):
    """Lista os membros de um grupo
    # roles = MANAGER, OWNER, MEMBER

    https://developers.google.com/admin-sdk/directory/v1/reference/members/
    """
    try:    
        # Permite continuar as paginações enquanto houver algum token   
        next_page = True
        token_paginacao = '' 
        members_list = []
        while next_page:    
            # Coleta resultados do GoogleApps        
            results = members_method.list(             # Consulta na API
                groupKey=groupKey, 
                pageToken=token_paginacao, 
                #maxResults=maxResults, 
                roles=roles, 
                includeDerivedMembership=includeDerivedMembership
                ).execute()
            
            members = results['members']
            token_paginacao = results.get('nextPageToken')
                    
            if not token_paginacao:
                next_page = False
                
            if not members:
                print("Nenhum membro encontrado.")
            else:        
                # Armazena usuários na lista
                for member in members:
                    members_list.append(member)
        print(len(members_list), 'usuário(s) armazenados em memória.\n')
        return members_list

    except:
        print('Verifique o preenchimento correto dos parâmetros')        
        return False
        

############################################################################################
# Method: groups()
############################################################################################

groups_method = admin_directory_v1.groups()

def groups_insert(email, description=None, name=None):
    '''Cria novo grupo
    # https://developers.google.com/resources/api-libraries/documentation/admin/directory_v1/python/latest/admin_directory_v1.groups.html#insert
    # https://developers.google.com/admin-sdk/directory/v1/reference/groups/insert
    
    '''
    
    body = { # JSON template for Group resource in Directory API.
             # "nonEditableAliases": [ nonEditableAliases ] # List of non editable aliases (Read-only),
             # "kind": "admin#directory#group", # Kind of resource this is.
             "description": description, # Description of the group
             "name": name, # Group name
             # "adminCreated": True or False, # Is the group created by admin (Read-only) *
             # "directMembersCount": "A String", # Group direct members count
             # "id": "A String", # Unique identifier of Group (Read-only)
             # "etag": "A String", # ETag of the resource.
             "email": email, # Email of Group
             #"aliases": [ aliases ] # List of aliases (Read-only)
            }
    
    try:
        results = groups_method.insert(             # Consulta na API
                body = body
                ).execute()
        return results

    except:
        print('Grupo já existe?')                
        return False
    

def groups_get(groupKey, fields=None):
    # Retorna as propriedades do grupo
    # https://developers.google.com/apis-explorer/#search/groups/m/admin/directory_v1/directory.groups.get

    try:
        results = groups_method.get(             # Consulta na API
                groupKey = groupKey
                ).execute()
        return results

    except:
        print('Verifique o preenchimento correto dos parâmetros')                
        return False

def groups_list(customer=None, userKey=None, domain=None, fields=None):
    # Lista os grupos que o usuário é membro
    # https://developers.google.com/admin-sdk/directory/v1/reference/groups/list
    # https://developers.google.com/resources/api-libraries/documentation/admin/directory_v1/python/latest/admin_directory_v1.groups.html#list
    # 
    # Parâmetros da API
    # list(customer=None, orderBy=None, domain=None, pageToken=None, maxResults=None, sortOrder=None, query=None, userKey=None)
    
    try:
        # Permite continuar as paginações enquanto houver algum token   
        next_page = True
        token_paginacao = '' 
        groups_list = []
        while next_page:    
            # Coleta resultados do GoogleApps        
            results = groups_method.list(             # Consulta na API
                customer=customer,
                domain=domain,
                pageToken=token_paginacao, 
                userKey=userKey, 
                ).execute()
            
            if not 'groups' in results:
                print('Nenhum grupo encontrado.')
                return False
            
            groups = results['groups']         
            token_paginacao = results.get('nextPageToken')
                    
            if not token_paginacao:
                next_page = False
                
            # Armazena grupos na lista
            for group in groups:
                groups_list.append(group['email'])
                    
        print(len(groups_list), 'grupo(s) armazenados em memória.\n')
        return groups_list, len(groups_list)
        
    except:        
        print('Verifique o preenchimento correto dos parâmetros')
        return False

def groups_delete(userKey, fields=None):
    # Deleta grupos
    # https://developers.google.com/admin-sdk/directory/v1/reference/groups/delete
    try:
        results = groups_method.delete(             # Consulta na API
                userKey = userKey
                ).execute()
        return results
    
    except:
        print('Verifique o preenchimento correto dos parâmetros')                
        return False
    
    
def groups_aliases_delete(groupKey, alias, fields=None):
    # Deleta alias de grupos
    # http://googleapis.github.io/google-api-python-client/docs/dyn/admin_directory_v1.groups.aliases.html
    try:
        results = groups_method.aliases().delete(             # Consulta na API
                groupKey = groupKey, 
                alias = alias
                ).execute()
        
        print('Alias excluído com sucesso.')                
        return results
    
    except:
        print('Alias não existe.')                
        return False

def groups_update(groupKey, newEmail, fields=None):
    # https://developers.google.com/apis-explorer/#search/delete%20user/m/admin/directory_v1/directory.users.undelete
    
    body = {
        #"nonEditableAliases": [],
        #"kind": "admin#directory#group", # Kind of resource this is.
        #"description": "A String", # Description of the group
        #"name": "A String", # Group name
        #"adminCreated": True or False, # Is the group created by admin (Read-only) *
        #"directMembersCount": "A String", # Group direct members count
        #"id": "A String", # Unique identifier of Group (Read-only)
        #"etag": "A String", # ETag of the resource.
        "email": newEmail, # Email of Group
        #"aliases": [] 
    }
    
    try:
        results = groups_method.update(             # Consulta na API
                groupKey = groupKey,
                body = body
                ).execute()
        
        print('Nome do grupo alterado.')        
        return results
    
    except:
        print('Verifique o preenchimento correto dos parâmetros')        
        return False


############################################################################################
# Method: users()
############################################################################################

users_method = admin_directory_v1.users()

def users_list(orderBy=None, domain=None, projection=None, query=None, event=None, showDeleted=None, pageToken=None, sortOrder=None, maxResults=None, customer=None, customFieldMask=None, viewType=None, fields=None): 
    """Lista usuários do GApps, extrai dados de usuários da directory.users.list
    
    # Python Docs:
      http://googleapis.github.io/google-api-python-client/docs/dyn/admin_directory_v1.users.html#list
    # API:
      https://developers.google.com/admin-sdk/directory/v1/reference/users/list
    
    # Queries para busca de usuários:
        https://developers.google.com/admin-sdk/directory/v1/guides/search-users
        
        # Ex:
          query='Daniel' # busca em givenName, familyName e email
          query='isSuspended=false'
          query="name:'Jane'"
          query='email:admin*'
          query='isSuspended=true and name:"andre"'
          fields="users/primaryEmail,nextPageToken", query='email:"andre.almeida@derosemethod.org"
    #    
    # DICA: Se usar o campo fields para buscar campos específicos,
    #       lembre-se de inserir nextPageToken
    #       
    #       Ex: fields="users/name, nextPageToken" 
    #           fields="users/email, nextPageToken" 
    #
    """
     
    if not projection:
        projection = 'full'
    if not orderBy:
        orderBy = 'email'
    if not sortOrder:
        sortOrder = 'ASCENDING'
    if not customer:
        customer = 'my_customer'
        print('Assumindo my_customer')
    if not customFieldMask:
        customFieldMask 
    if not sortOrder:
        sortOrder='ASCENDING'
    if not viewType:
        viewType='admin_view'   
    
    # Exibe lista de domínios disponíveis. 
    # Apenas para conveniência.
    if not domain:
        domains_list()
    
    # INICIA ESTRUTURA DE PAGINAÇÃO
    # Permite continuar as paginações enquanto houver algum token   
    next_page = True
    token_paginacao = '' 
    users_list = []
    
    try:
        while next_page:    
            # Coleta resultados do GoogleApps
            results = users_method.list(
                orderBy=orderBy, 
                domain=domain, 
                projection=projection, 
                query=query, # Manual: https://developers.google.com/admin-sdk/directory/v1/guides/search-users
                event=event, 
                showDeleted=showDeleted,
                pageToken=token_paginacao, 
                sortOrder=sortOrder, 
                maxResults=maxResults, 
                customer=customer, 
                customFieldMask=customFieldMask, 
                viewType=viewType,
                fields=fields
                ).execute()
            
            print('.', end='')
            token_paginacao = results.get('nextPageToken')
            if not token_paginacao:
                next_page = False
                
            # Constrói a lista somando os resultados de results
            users_list += results['users']

            # FIM DA ESTRUTURA DE PAGINAÇÃO
            
        print('\n' + str(len(users_list)) + ' usuário(s) armazenados em memória.')
        return users_list
    except:
        print('Erro.')
        return False

def users_insert(fields=None): # Cria usuário

    # Email Institucional e nome do usuário
    domain = input('Digite o nome do domínio ou [Enter para @derosemethod]: ') or '@derosemethod.org'
    primaryEmail = input('Digite a primeira parte do email formato nome.sobrenome: ') + domain
    givenName = input('Digite o nome: ')
    familyName = input('Digite o sobrenome: ')
    print('Gerando nova senha...')
    password = ''.join(random.choice(string.ascii_letters + string.digits + '!#$%&()*+,-.:;<=>?@[\]_{}') for _ in range(8))
    print('Senha: ' + password)
    changePasswordAtNextLogin = True
    
    
    # Email pessoal do usuário
    secundaryEmail = input('Digite o email pessoal para recuperação de senha: ') # não tenho certeza se realmente recupera
    customType = 'pessoal'
    
    # Insere usuários dentro da organização Students. 
    # Isto permite definir parâmetros para as aplicações utilizadas para todos os alunos.
    print(orgunits_list(orgUnitPath=None))
    orgUnitPath = input('Insira a organização. [Enter] para /Students: ') or '/Students'         
    
    # Define variáveis de grupo
    while True:
        print(groups_list(customer='my_customer'))
        group = input('Defina o grupo do usuário. [ENTER] para IGNORAR. Exemplo: credenciados@derosemethod.org\n> ')
        if not group:
            break
        groupRole = input('Defina a permissão. [ENTER] para MEMBER\n\nOPÇÕES: \nMANAGER, \nOWNER, \nMEMBER\n\n> ') or 'MEMBER'    # MANAGER, OWNER, MEMBER
        
    body = {'name': 
                {
                  'familyName': familyName,
                  'givenName': givenName
                },
            'password': password,
            'primaryEmail': primaryEmail,
            'changePasswordAtNextLogin': changePasswordAtNextLogin,
            'orgUnitPath': orgUnitPath,
            'organizations': 
                [{
                  'name': 'DeRose Method Testando'
                }],
            'emails':
                [{
                  'type': 'custom',
                  'address': secundaryEmail,
                  'customType': customType
                }],        
            'fields': fields}

    try:
        print('Criando usuário...')        
        results = users_method.insert(                     # Consulta na API
            fields='primaryEmail,password,changePasswordAtNextLogin,creationTime,emails,name,orgUnitPath,organizations',
            body=body
            ).execute()
        members_insert(group, primaryEmail, groupRole)
        
        return results
    
    except:
        return False

def users_get(userKey, fields=None):
    # https://developers.google.com/apis-explorer/#search/delete%20user/m/admin/directory_v1/directory.users.get
    try:
        results = users_method.get(             # Consulta na API
                userKey = userKey,
                fields = fields
                ).execute()
        return results
    
    except:
        return False
    
def users_delete(userKey, fields=None):
    # https://developers.google.com/apis-explorer/#search/delete%20user/m/admin/directory_v1/directory.users.delete
    try:
        results = users_method.delete(             # Consulta na API
                userKey = userKey,
                fields = fields
                ).execute()
        print('Usuário deletado com sucesso.')  
        return results
    
    except:
        return False

def users_undelete(userKey, orgUnitPath=None, fields=None):
    # Recupera usuários deletados.
    # 
    # IMPORTANTE!
    # userKey = id do usuário
    # Para recuperar a id, necessário listar com showDeleted=True
    # Ex.: users_list(domain='derosemethod.org', showDeleted=True)
    # 
    # https://developers.google.com/apis-explorer/#search/delete%20user/m/admin/directory_v1/directory.users.undelete
    
    body = {
        "orgUnitPath": orgUnitPath,
        "fields": fields
    }
    
    try:
        results = users_method.undelete(             # Consulta na API
                userKey = userKey,
                body = body
                ).execute()
        print('Usuário recuperado com sucesso.')        
        return results
    
    except:
        print('Verifique o preenchimento correto dos parâmetros')        
        return False


def users_update(userKey, 
                 primaryEmail=None, 
                 givenName=None, 
                 familyName=None, 
                 suspended=None, 
                 password=None, 
                 orgUnitPath=None, 
                 addresses=None, 
                 posixAccounts=None, 
                 phones=None, 
                 locations=None, 
                 recoveryPhone=None, 
                 keywords=None, 
                 aliases=None, 
                 nonEditableAliases=None, 
                 archived=None, 
                 deletionTime=None, 
                 relations=None, 
                 includeInGlobalAddressList = None,
                 languages = None,
                 ims=None,
                 etag=None,
                 externalIds=None, 
                 ipWhitelisted=None, 
                 sshPublicKeys=None, 
                 customSchemas=None, 
                 emails=None,
                 organizations=None, 
                 kind=None, 
                 hashFunction=None, 
                 fullName=None, 
                 gender=None, 
                 notes=None, 
                 websites=None, 
                 changePasswordAtNextLogin=None, 
                 recoveryEmail=None,
                 fields=None):

    ''' Atualiza dados do usuário
    # https://developers.google.com/apis-explorer/#search/user%20update/m/admin/directory_v1/directory.users.update
    # https://developers.google.com/resources/api-libraries/documentation/admin/directory_v1/python/latest/admin_directory_v1.users.html#update
    
    '''
    
    body = { # JSON template for User object in Directory API.
        "addresses": addresses,
        "posixAccounts": posixAccounts,
        "phones": phones,
        "locations": locations,
        #"isDelegatedAdmin": True or False, # Boolean indicating if the user is delegated admin (Read-only)
        "recoveryPhone": recoveryPhone, # Recovery phone of the user. The phone number must be in the E.164 format, starting with the plus sign (+). Example: +16506661212.
        "suspended": suspended, # Indicates if user is suspended.
        "keywords": keywords,
        #"id": "A String", # Unique identifier of User (Read-only)
        "aliases": [ ],
        "nonEditableAliases": [ ],
        "archived": archived, #True or False, # Indicates if user is archived.
        "deletionTime": deletionTime,
        #"suspensionReason": "A String", # Suspension reason if user is suspended (Read-only)
        #"thumbnailPhotoUrl": "A String", # Photo Url of the user (Read-only)
        #"isEnrolledIn2Sv": True or False, # Is enrolled in 2-step verification (Read-only)
        #"isAdmin": True or False, # Boolean indicating if the user is admin (Read-only)
        "relations": relations,
        "includeInGlobalAddressList": includeInGlobalAddressList, #True or False, # Boolean indicating if user is included in Global Address List
        "languages": languages,
        "ims": ims,
        "etag": etag, # ETag of the resource.
        #"lastLoginTime": "A String", # User's last login time. (Read-only)
        "orgUnitPath": orgUnitPath, # OrgUnit of User
        #"agreedToTerms": True or False, # Indicates if user has agreed to terms (Read-only)
        "externalIds": externalIds,
        "ipWhitelisted": ipWhitelisted, #True or False, # Boolean indicating if ip is whitelisted
        "sshPublicKeys": sshPublicKeys,
        "customSchemas": customSchemas, # { "a_key": { "a_key": "",},},
        #"isEnforcedIn2Sv": True or False, # Is 2-step verification enforced (Read-only)
        #"isMailboxSetup": True or False, # Is mailbox setup (Read-only)
        "primaryEmail": primaryEmail, # username of User
        "password": password, # User's password
        "emails": emails,
        "organizations": organizations,
        "kind": kind, #"admin#directory#user", # Kind of resource this is.
        "hashFunction": hashFunction, # "A String", # Hash function name for password. Supported are MD5, SHA-1 and crypt
        "name": { # JSON template for name of a user in Directory API. # User's name
          "givenName": givenName, # First Name
          "fullName": fullName, # Full Name
          "familyName": familyName, # Last Name
        },        
        "gender": gender,
        "notes": notes,
        #"creationTime": "A String", # User's G Suite account creation time. (Read-only)
        "websites": websites,
        "changePasswordAtNextLogin": changePasswordAtNextLogin, # Boolean indicating if the user should change password in next login
        "recoveryEmail": recoveryEmail, # Recovery email of the user.
        #"customerId": customerId, # CustomerId of User (Read-only)
        #"thumbnailPhotoEtag": "A String", # ETag of the user's photo (Read-only)
        "fields": fields
      }
    
    results = users_method.update(
        userKey=userKey,
        body=body
        ).execute()

    print(results)
    return results


############################################################################################
# Method: schemas()
############################################################################################

schemas_method = admin_directory_v1.schemas()

def schemas_list(fields=None):
    '''Lista os campos customizados.
    https://developers.google.com/admin-sdk/directory/v1/reference/schemas/list
    ''' 
    
    try:
        results = schemas_method.list(             # Consulta na API
                customerId = "my_customer",
                fields = fields
                ).execute()
        return results
    
    except:
        return False
    
    
def schemas_update(customerId, schemaKey, body):
    
    '''Faz update nos campos customizados do GApps
    # http://googleapis.github.io/google-api-python-client/docs/dyn/admin_directory_v1.schemas.html

    ### IMPORTANTE! ###
    Não implementado para evitar desastres.
    Se for necessário adicionar ou alterar, faça manualmente utilizando os campos do body abaixo.
    Utilize preferencialmente este link:
    #https://developers.google.com/admin-sdk/directory/v1/reference/schemas/update
    '''

    try:
        results = schemas_method.update(             # Consulta na API
                customerId = "my_customer",
                schemaKey = "k8cehRe6TwGepFkYsSNHcA==",
                body = {
                        "kind": "admin#directory#schema",
                        "schemaId": "k8cehRe6TwGepFkYsSNHcA==",
                        "etag": "\"enlFCt4L0-k8PoIKzRNl5h_fsrc/wqp6Yo7nNDO3-pIuEYzifb-_xXI\"",
                        "schemaName": "DeROSEMethod",
                        "displayName": "Bureau - DeROSE Method",
                        "fields": [
                         {
                          "kind": "admin#directory#schema#fieldspec",
                          "fieldId": "tZEN_HwsQhyWavHtP3jRJA==",
                          "etag": "\"enlFCt4L0-k8PoIKzRNl5h_fsrc/G8gSujhf8r70p8ZJvBblYnkEfUU\"",
                          "fieldType": "STRING",
                          "fieldName": "school",
                          "displayName": "Escola"
                         },
                         {
                          "kind": "admin#directory#schema#fieldspec",
                          "fieldId": "G421DLCQRrSlRxU68_jgUA==",
                          "etag": "\"enlFCt4L0-k8PoIKzRNl5h_fsrc/dUizeiqWK-ATu_ui2ajTEreqWo4\"",
                          "fieldType": "STRING",
                          "fieldName": "monitor",
                          "displayName": "Monitor"
                         },
                         {
                          "kind": "admin#directory#schema#fieldspec",
                          "fieldId": "Z5EwZAl1QZuavkmANYsPkg==",
                          "etag": "\"enlFCt4L0-k8PoIKzRNl5h_fsrc/8ufjVNsPtiKKsg-9Z80TWTSMZaE\"",
                          "fieldType": "STRING",
                          "fieldName": "supervisor",
                          "displayName": "Supervisor"
                         },
                         {
                          "kind": "admin#directory#schema#fieldspec",
                          "fieldId": "hfEQDVDyRVm1aT5J03DW0A==",
                          "etag": "\"enlFCt4L0-k8PoIKzRNl5h_fsrc/MAM5iM1kDRdym18XTAb-CPiPX7Y\"",
                          "fieldType": "DATE",
                          "fieldName": "birthday",
                          "displayName": "Aniversario"
                         }
                        ]
                       }
                ).execute()
        return results
    
    except:
        return False



# Coloque um breakpoint aqui para usar as funções no Debug Console do Wing Pro.
print('''--- BUREAU DE TECNOLOGIA --- Usando: Módulo GApps Admin''')