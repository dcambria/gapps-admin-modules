#!/usr/bin/env python3

# Implementação de vários métodos do Google Admin Directory API
# para Administradores GApps.
#
#
# Por: Daniel Cambría
# Criado em: 2020-02-03
# Última revisão:2020-02-12
# Versão: 0.1
# 
# Funções comentadas usando Docstrings https://www.datacamp.com/community/tutorials/docstrings-python#first-head

# Importa dependência de autenticação do Google APIs
from apiclient import discovery
from module_gappsapi_superadminauth import get_google_credentials
import httplib2


# Estes dois parâmetros devem ser iguais aos que foram setados nos IDs do cliente OAuth 2.0 
# https://console.cloud.google.com/apis/credentials
# Baixe o arquivo antes e salve no diretório do aplicativo
# Sete o mesmo nome em CLIENT_SECRET_FILE
CLIENT_SECRET_FILE = 'client_secrets.json'
APPLICATION_NAME = 'GAuth - Bureau'

# Define o diretório e arquivo do arquivo de credentials
# Este será o nome do arquivo de retorno do Google que credenciadá a conexão
credential_file = 'googleapis.com-python-gauth-reports-bureau.json'

# Escopos permitidos para o credentials_file trabalhar.
# Se modificar o escopo, delete o arquivo setado em credential_file
# Deve ser semelhante ao definido em https://console.cloud.google.com/apis/credentials
scopes = ['https://www.googleapis.com/auth/admin.reports.audit.readonly']

# Recebe as credenciais OAuth2 do Google
credentials = get_google_credentials(scopes, CLIENT_SECRET_FILE, APPLICATION_NAME, credential_file)
http = credentials.authorize(httplib2.Http())

# Admin Reports API
# https://developers.google.com/resources/api-libraries/documentation/admin/reports_v1/python/latest/
# https://developers.google.com/admin-sdk/reports/v1/reference/

# Lista de APIs possíveis
# https://github.com/googleapis/google-api-python-client/blob/master/docs/dyn/index.md

# Conecta com a API Reports
admin_reports_v1 = discovery.build('admin', 'reports_v1', http=http)

# Váriáveis para testes rápidos
customer = "my_customer"


############################################################################################
# Method: activities()
############################################################################################

activities_method = admin_reports_v1.activities()

def activities_list(userKey, applicationName,
                    startTime=None,
                    filters=None,
                    eventName=None,
                    actorIpAddress=None,
                    pageToken=None,
                    orgUnitID=None,
                    maxResults=None,
                    endTime=None,
                    customerId=None,
                    fields=None):
    
    '''Retrieves a list of activities for a specific customer's account and application such as the Admin console application or the Google Drive application. For more information, see the guides for administrator and Google Drive activity reports. For more information about the activity report's parameters, see the activity parameters reference guides.
    # https://developers.google.com/resources/api-libraries/documentation/admin/reports_v1/python/latest/admin_reports_v1.activities.html#list
    # https://developers.google.com/admin-sdk/reports/v1/reference/activities/list
    
    '''
    
    try:
        results = activities_method.list(
                userKey = userKey, # admin, calendar userKey ou all
                applicationName = applicationName, #access_transparency, admin, calendar, chat, drive, gcp, gplus, groups, groups_enterprise, jamboard, login, meet, mobile, rules, saml, token, user_accounts
                startTime=startTime,
                filters=filters,
                eventName=eventName,
                actorIpAddress=actorIpAddress,
                pageToken=pageToken,
                orgUnitID=orgUnitID,
                maxResults=maxResults,
                endTime=endTime,
                customerId=customerId,
                fields=fields
                ).execute()
        
        return results
    
    except:
        print('Erro. Verifique preenchimento dos campos')                
        return False
    

# Coloque um breakpoint aqui para usar as funções no Debug Console do Wing Pro.
print('''--- BUREAU DE TECNOLOGIA --- Usando: Módulo GApps Reports''')