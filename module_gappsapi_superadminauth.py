#!/usr/bin/env python3

# Realiza a autenticação utilizando browser.
# Somente superadmins terão acesso a todos os escopos.
# Caso necessário, consulte https://developers.google.com/identity/protocols/OAuth2ServiceAccount#delegatingauthority
# para autenticação server-to-server usando Service Account.
#
#
# Por: Daniel Cambría
# Criado em: 2020-02-03
# Última revisão: 2020-02-04
# Versão: 0.2


import os

from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

# Gera credenciais necessárias para conexão às Google APIs
def get_google_credentials(scopes, CLIENT_SECRET_FILE, APPLICATION_NAME, credential_file):   
    """Gets valid user credentials from storage.
    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.
    """
    
    # Prepara e grava o credential_file no diretório padrão do computador
    home_dir = os.path.expanduser('~')                              # Comportamento de caminho esperado: ~/
    credential_dir = os.path.join(home_dir, '.credentials')         # Diretório para gravar o credentials_file. Comportamento esperado: ~/.credentials/credential_file.json    
    if not os.path.exists(credential_dir):                          # Verifica se o diretório existe
        os.makedirs(credential_dir)                                 # Cria diretório
    credential_path = os.path.join(credential_dir, credential_file)
    
    store = Storage(credential_path)
    credentials = store.get()                                       # retorna None se não encontra o credential.file

    # Gera o arquivo de credentials quando não existe ou é inválido
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, scopes, "Arquivo " + credential_file + "não encontrado.\nCriando um novo arquivo...")   # 
        flow.user_agent = APPLICATION_NAME
        
        print('\n Gravando credenciais em:\n' + credential_path)
        credentials = tools.run_flow(flow, store)

    return credentials